function lenval = getarrowlength(val,maxval,minlen,maxlen)

x = [0, maxval/4,maxval];%-4:4;
y = [minlen, minlen + (maxlen-minlen)*0.05, maxlen];

cs = spline(x,[0 y maxlen/maxval]);
xx = linspace(0,maxval);
% hold on;
% plot(x,y,'o',xx,ppval(cs,xx),'-');
% plot(val,ppval(cs,val),'rs');
lenval = ppval(cs,val);

end