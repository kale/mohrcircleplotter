function plotMohrCircles(ah,sigX,sigY,sigXY,sigZZ,units,thetarot)
%% Plot Mohr's circles

[theta_Princ,sig_princX,sig_princY] = getPrincipalStresses(sigX,sigY,sigXY);
maxval = max(abs([sig_princX,sig_princY,sigZZ]));

thmat = linspace(0,2*pi,100);
% plot coordinates
cla(ah);
hold(ah,'on');
r_mohr = sqrt( ((sigX-sigY)/2)^2 + sigXY^2);
c_mohr = (sigX+sigY)/2;
hA = plot(ah,sigX,-sigXY,'ks');
hA.MarkerSize = 12;
hA.LineWidth = 2;
hA.MarkerFaceColor = 'r';
hB = plot(ah,sigY,sigXY,'ks');
hB.MarkerSize = 12;
hB.LineWidth = 2;
hB.MarkerFaceColor = 'r';
hC = plot(ah,c_mohr,0,'ko');
hC.MarkerSize = 6;
hC.LineWidth = 2;
hC.MarkerFaceColor = 'r';

hAB = plot(ah,[sigX,sigY],[-sigXY,sigXY],'k-');
hAB.LineWidth = 2.0;
offset = 0.05 * maxval;
text(ah,sigX + offset,-sigXY,'A','FontSize',16);
text(ah,sigY + offset, sigXY,'B','FontSize',16);

if thetarot~= 0
    [sigX_p,sigY_p,sigXY_p] = getRotatedStressComponents(sigX,sigY,sigXY,thetarot);
    hApBp = plot(ah,[sigX_p,sigY_p],[-sigXY_p,sigXY_p],'k--');
    hApBp.LineWidth = 1.0;
    offset = 0.05 * maxval;
    text(ah,sigX_p + offset,-sigXY_p,'A''','FontSize',16);
    text(ah,sigY_p + offset, sigXY_p,'B''','FontSize',16);
end

hmohrXY = plot(ah,c_mohr + r_mohr*cos(thmat), r_mohr*sin(thmat),'-');
hmohrXY.LineWidth = 2;
hmohrXY.Color = 'r';

sig_i = c_mohr + r_mohr;
sig_j = c_mohr - r_mohr;
sig_k = sigZZ;

c_mohrXZ = (sig_i + sig_k)/2;
r_mohrXZ = abs(sig_i - sig_k)/2;
hmohrXZ = plot(ah,c_mohrXZ + r_mohrXZ*cos(thmat), r_mohrXZ*sin(thmat),'-');
hmohrXZ.LineWidth = 1.5;
hmohrXZ.Color = 'k';

c_mohrYZ = (sig_j + sig_k)/2;
r_mohrYZ = abs(sig_j - sig_k)/2;
hmohrYZ = plot(ah,c_mohrYZ + r_mohrYZ*cos(thmat), r_mohrYZ*sin(thmat),'-');
hmohrYZ.LineWidth = 1.5;
hmohrYZ.Color = 'k';

princstressmat = sort([sig_i;sig_j;sig_k]);
sig_P1 = princstressmat(3);
sig_P2 = princstressmat(2);
sig_P3 = princstressmat(1);
h1 = plot(ah,sig_P1,0,'ko');
h1.MarkerSize = 10;
h1.MarkerFaceColor = 'k';
h2 = plot(ah,sig_P2,0,'ko');
h2.MarkerSize = 10;
h2.MarkerFaceColor = 'k';
h3 = plot(ah,sig_P3,0,'ko');
h3.MarkerSize = 10;
h3.MarkerFaceColor = 'k';

offset = 0.05 * maxval;
text(ah,sig_P1 + offset,-offset,'\sigma_1','FontSize',16,'interpreter','tex');
text(ah,sig_P2 + offset,-offset,'\sigma_2','FontSize',16,'interpreter','tex');
text(ah,sig_P3 + offset,-offset,'\sigma_3','FontSize',16,'interpreter','tex');

ah.XAxisLocation = "origin";
ah.YAxisLocation = "origin";
ah.XLabel.String = sprintf('\\sigma [%s]',units);
ah.XLabel.FontSize = 12;
ah.YLabel.String = sprintf('\\tau [%s]',units);
ah.YLabel.FontSize = 12;

hold(ah,'off');
end