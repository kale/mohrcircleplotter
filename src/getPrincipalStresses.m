function [theta_Princ,sig_princX,sig_princY] = getPrincipalStresses(sigX,sigY,sigXY)

% Calculate the principal stresses and principal orientations
theta_Princ = 0.5 * atan2(2*sigXY,sigX - sigY); % [rad]
sig_princX  = (sigX + sigY)/2 + (sigX - sigY)/2 * cos(2*theta_Princ) + sigXY * sin(2*theta_Princ);
sig_princY = (sigX + sigY)/2 + (sigX - sigY)/2 * cos(2*(theta_Princ + pi/2)) + sigXY * sin(2*(theta_Princ+pi/2));

end