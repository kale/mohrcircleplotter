function plotStressElement(ah,sigX,sigY,sigXY,sigZZ,units,thetarot)
% Plot stress element at thetarot
[theta_Princ,sig_princX,sig_princY] = getPrincipalStresses(sigX,sigY,sigXY);

maxval = max(abs([sig_princX,sig_princY,sigZZ]));
maxlen = 0.75;
minlen = 0.3;

[sigX_p,sigY_p,sigXY_p] = getRotatedStressComponents(sigX,sigY,sigXY,thetarot);
theta_X   = thetarot * pi/180; % [rad]

%% Plotting components on rotated stress element
cla(ah);
hold(ah,"on");

ah.Visible = 'off';
ah.XLim = [-1.75, 1.75];
ah.YLim = [-1.75, 1.75];

% Plot the stress element in the global ref coords
cornerangles = [-3*pi/4+theta_X , -pi/4+theta_X, pi/4+theta_X, 3*pi/4+theta_X];
sqcoords = zeros(2,4);
sqcoords(1,:) = cos(cornerangles);
sqcoords(2,:) = sin(cornerangles);
sqh = patch(ah,sqcoords(1,:),sqcoords(2,:),'w');
sqh.EdgeColor = 'k';
sqh.LineWidth = 2.0;

arrowlinewidth = 4.0;
arrowcolor     = 'r';
arrowcolorXY   = 'b';
arrowheadsize  = 0.25;

offset = 0.1;

% sigX_p arrows
if abs(sigX_p) > 1E-2
sigXp_angles = [0 , pi] + theta_X;
sigXpcoords = zeros(2,2);
sigXpcoords(1,:) = (1/sqrt(2) + offset)*cos(sigXp_angles);
sigXpcoords(2,:) = (1/sqrt(2) + offset)*sin(sigXp_angles);
sigXpvec = zeros(2,2);
sigX_plen = getarrowlength(abs(sigX_p),maxval,minlen,maxlen);
sigXpvec(1,:) = sign(sigX_p) * sigX_plen*cos(sigXp_angles);
sigXpvec(2,:) = sign(sigX_p) * sigX_plen*sin(sigXp_angles);
%sigXXvec    = [-sigX, sigX; 0, 0]*scalefactor;
if sign(sigX_p) <= 0
    arrowdir = 1;
else
    arrowdir = 0;
end
quiver_tri(sigXpcoords(1,:),sigXpcoords(2,:),...
    sigXpvec(1,:),sigXpvec(2,:),...
    arrowheadsize,22.5,arrowlinewidth,arrowcolor,arrowdir,ah);
end

% sigY_p arrows
if abs(sigY_p) > 1E-2
sigYp_angles = [pi/2 , 3*pi/2] + theta_X;
sigYpcoords = zeros(2,2);
sigYpcoords(1,:) = (1/sqrt(2) + offset)*cos(sigYp_angles);
sigYpcoords(2,:) = (1/sqrt(2) + offset)*sin(sigYp_angles);
sigYpvec = zeros(2,2);
sigY_plen = getarrowlength(abs(sigY_p),maxval,minlen,maxlen);
sigYpvec(1,:) = sign(sigY_p) * sigY_plen*cos(sigYp_angles);
sigYpvec(2,:) = sign(sigY_p) * sigY_plen*sin(sigYp_angles);
if sign(sigY_p) <= 0
    arrowdir = 1;
else
    arrowdir = 0;
end
quiver_tri(sigYpcoords(1,:),sigYpcoords(2,:),...
    sigYpvec(1,:),sigYpvec(2,:),...
    arrowheadsize,22.5,arrowlinewidth,arrowcolor,arrowdir,ah);
end

% sigXY arrows
if abs(sigXY_p) > 1E-2
    % [0, pi]
    sigXYp_angles = [0, pi] + theta_X;

    sigXYpcoords = zeros(2,2);
    sigXYpcoords(1,:) = (1/sqrt(2) + offset)*cos(sigXYp_angles);
    sigXYpcoords(2,:) = (1/sqrt(2) + offset)*sin(sigXYp_angles);

    sigXYpvec = zeros(2,2);
    tauXY_plen = getarrowlength(abs(sigXY_p),maxval,minlen,maxlen);
    sigXYpvec(1,:) = sign(sigXY_p) * tauXY_plen*cos(sigXYp_angles + pi/2);
    sigXYpvec(2,:) = sign(sigXY_p) * tauXY_plen*sin(sigXYp_angles + pi/2);

    quiver_tri(sigXYpcoords(1,:),sigXYpcoords(2,:),...
        sigXYpvec(1,:),sigXYpvec(2,:),...
        arrowheadsize,22.5,arrowlinewidth,arrowcolorXY,1/2,ah);

    % [pi/2, 3*pi/2]
    sigXYp_angles = [pi/2, 3*pi/2] + theta_X;

    sigXYpcoords = zeros(2,2);
    sigXYpcoords(1,:) = (1/sqrt(2) + offset)*cos(sigXYp_angles);
    sigXYpcoords(2,:) = (1/sqrt(2) + offset)*sin(sigXYp_angles);

    sigXYpvec = zeros(2,2);
    tauXY_plen = getarrowlength(abs(sigXY_p),maxval,minlen,maxlen);
    sigXYpvec(1,:) = -sign(sigXY_p) * tauXY_plen*cos(sigXYp_angles + pi/2);
    sigXYpvec(2,:) = -sign(sigXY_p) * tauXY_plen*sin(sigXYp_angles + pi/2);

    quiver_tri(sigXYpcoords(1,:),sigXYpcoords(2,:),...
        sigXYpvec(1,:),sigXYpvec(2,:),...
        arrowheadsize,22.5,arrowlinewidth,arrowcolorXY,1/2,ah);

end

% Add labels for stresses
if thetarot ~= 0
    facelabelX = 'A''';
    facelabelY = 'B''';
    stressstatetxt = sprintf('\\sigma_{x''} = %3.1f %s\n\\sigma_{y''} = %3.1f %s\n\\tau_{x''y''} = %3.1f %s\n\\sigma_{z} = %3.1f %s'...
    ,sigX_p, units,sigY_p, units, sigXY_p, units,sigZZ, units);
else
    facelabelX = 'A';
    facelabelY = 'B';
    stressstatetxt = sprintf('\\sigma_{x} = %3.1f %s\n\\sigma_{y} = %3.1f %s\n\\tau_{xy} = %3.1f %s\n\\sigma_{z} = %3.1f %s'...
    ,sigX, units,sigY, units, sigXY, units,sigZZ, units);
end
text(ah,1.25,-1.25,stressstatetxt,'FontSize',12,'interpreter','tex');
offsetx = 0.3;
offsety = 0.15;
text(ah,(1/sqrt(2) - offsetx)*cos(theta_X),(1/sqrt(2) - offsetx)*sin(theta_X),facelabelX,'FontSize',16,'interpreter','tex');
text(ah,(1/sqrt(2) - offsety)*cos(theta_X + pi/2),(1/sqrt(2) - offsety)*sin(theta_X + pi/2),facelabelY,'FontSize',16,'interpreter','tex');
end

