function [sigX_p,sigY_p,tauXY_p] = getRotatedStressComponents(sigX,sigY,sigXY,thetarot)
theta_X   = thetarot * pi/180; % [rad]
theta_Y   = theta_X + pi/2; % [rad]

% Rotated componets
sigX_p = (sigX + sigY)/2 + (sigX - sigY)/2 * cos(2*theta_X) + sigXY * sin(2*theta_X);
sigY_p = (sigX + sigY)/2 + (sigX - sigY)/2 * cos(2*theta_Y) + sigXY * sin(2*theta_Y);
tauXY_p = sigXY * cos(2*theta_X) - (sigX - sigY)/2 * sin(2*theta_X);
end