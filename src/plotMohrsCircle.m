clc
clear
close all

addpath('./quiver_tri/');

% Input data
units = 'MPa';
sigXX = 100;
sigYY = 40;
sigXY = 40;
sigZZ = 0;
thetarot = 45.0;

fhse = figure(1); hold on;
fhse.Color = 'w';
fhse.Units = "centimeters";
fhse.Position = [2 2 15 14];
axis equal;
plotStressElement(fhse,sigXX,sigYY,sigXY,sigZZ,units,0.0)

fhse = figure(2); hold on;
fhse.Color = 'w';
fhse.Units = "centimeters";
fhse.Position = [2 2 15 14];
axis equal;
plotStressElement(fhse,sigXX,sigYY,sigXY,sigZZ,units,thetarot)

fhMohr = figure(3); hold on;
fhMohr.Color = 'w';
fhMohr.Units = "centimeters";
fhMohr.Position = [17 2 15 14];
axis equal;
plotMohrCircles(fhMohr,sigXX,sigYY,sigXY,sigZZ,units,thetarot)
